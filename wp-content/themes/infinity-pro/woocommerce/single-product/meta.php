<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php echo $product->get_categories( ', ', '<li class="posted_in">' . _n( '<svg class=\'icon\'><use xlink:href=\'#category\'></svg>', '<svg class=\'icon\'><use xlink:href=\'#category\'></svg>', $cat_count, 'woocommerce' ) . ' ', '</li>' ); ?>

	<?php echo $product->get_tags( ', ', '<li class="tagged_as">' . _n( '<svg class=\'icon\'><use xlink:href=\'#tag\'></svg>', '<svg class=\'icon\'><use xlink:href=\'#tag\'></svg>', $tag_count, 'woocommerce' ) . ' ', '</li>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</ul>
