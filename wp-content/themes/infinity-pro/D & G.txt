D & G

Header

Before header  with "Christmas gifts in the shop or Spend £100 and receive free shipping"

Fixed turning blue on scroll of browser.

Fixed header animated via transition (transition: all 0.5s ease-in-out 0s) on scroll - Not fixed but added to the bottom of the page & Moved to the top with transition (see before). No logo in the header until scroll. Logo on the front page.



Layout -

LOGO            |     MENU       |   SEARCH | MY ACCOUNT


2) Image
3) Categories
4) Latest Products
5) About blurb
6) Footer

-------------------------------------------------------
Handburger Icon

https://jonsuh.com/hamburgers/#accessibility

-------------------------------------------------------


SEARCH & PAGE:

Replace button with icon:

(Add Icon Inside Search Input Field)

http://wpsites.net/web-design/3-ways-to-customize-genesis-search-form-input-box-button/

https://sridharkatakam.com/expanding-search-bar-by-codrops-in-genesis/

http://rickrduncan.com/wordpress/dashicons-genesis-search-box

----------------------------

Create a searchpage.php

add_action( 'template_redirect', 'my_search_template_redirect', 1);

function my_search_template_redirect() {

  if ( is_search() ) {
    include( get_stylesheet_directory() . '/searchpage.php' );
    exit();
  }
}

add_action( 'template_redirect', 'my_search_template_redirect', 1);

Add either this plugin

https://wordpress.org/plugins/daves-wordpress-live-search/

or this page:

https://gist.githubusercontent.com/srikat/894f682c8b03443401b0/raw/2ebac61b5ba7b9e0a9f617cd256b8fca23e95ca7/search.php

or this page:

http://wpbeaches.com/add-a-search-again-to-a-no-results-page-in-genesis/



Before Footer -

SSL Icon
Powered by Pay Pal
Security, etc

Footer -

First Column:

Logo | Social Media

(If Room then menu repeated)

Second Column:

Tags


Third Colcumn:

Mailing List


Add Cookie support to the footer



//////////////////////////////////////////////////


WOOCOMMERCE:

Add a Continue Shopping button next to View Cart button:

/**
* Add Continue Shopping Button on Cart Page
* Add to theme functions.php file or Code Snippets plugin
*/

add_action( 'woocommerce_before_cart_table', 'woo_add_continue_shopping_button_to_cart' );
function woo_add_continue_shopping_button_to_cart() {
 $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );

 echo '<div class="woocommerce-message">';
 echo ' <a href="'.$shop_page_url.'" class="button">Continue Shopping →</a> Would you like some more goods?';
 echo '</div>';
}

-----------------------------------------------------

CATEGORY AND PRODUCTS SEPERATE LISTS:

Add clear left on the first product list item


https://code.tutsplus.com/tutorials/display-woocommerce-categories-subcategories-and-products-in-separate-lists--cms-25479

----------------------------------------------------
CATEGORY IMAGES

$prod_categories = get_terms( 'product_cat', array(
        'orderby'    => 'name',
        'order'      => 'ASC',
        'hide_empty' => true
    ));

    foreach( $prod_categories as $prod_cat ) :
        $cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
        $shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
        $term_link = get_term_link( $prod_cat, 'product_cat' );?>

        <a href="<?php echo $term_link; ?>"><img src="<?php echo $shop_catalog_img[0]; ?>" alt="<?php echo $prod_cat->name; ?>" /></a>

    <?php endforeach; wp_reset_query();

    http://wordpress.stackexchange.com/questions/200170/woocommerce-get-category-image-full-size
-----------------------------------------------------------
SHOP

Example of shop page:

http://demo.woothemes.com/?name=superstore&_ga=1.30367096.679804168.1473497360

Single Product Page:

https://woocommerce.com/woosidebars/


    -----------------------------------------------

    MISC (First and last class to widgets)

    https://sridharkatakam.com/add-first-last-numbered-classes-automatically-widgets-wordpress/

    Whitelabel Wordpress (add to functionality plugin)

---------------------------------