<?php
/**
 * Infinity Pro.
 *
 * This file adds the front page to the Infinity Pro Theme.
 *
 * @package Infinity
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/infinity/
 */


add_action( 'genesis_meta', 'front_page' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */		

function front_page() {

 //* Add front-page body class
		add_filter( 'body_class', 'infinity_body_class' );
		function infinity_body_class( $classes ) {

			$classes[] = 'front-page';

			return $classes;

		}

		//* Force full width content layout
		add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

		//* Remove breadcrumbs
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

		//* Remove the default Genesis loop
		remove_action( 'genesis_loop', 'genesis_do_loop' );

		add_action( 'genesis_loop', 'flexible' );

		//* Remove site footer widgets
		remove_theme_support( 'genesis-footer-widgets' );

		//* Remove entry-title filter
		add_filter( 'genesis_featured_page_title', 'infinity_title' );

		//* Remove team-member class
		remove_filter( 'genesis_attr_entry', 'infinity_widget_entry_open' );

		//* Enqueue scripts
		add_action( 'wp_enqueue_scripts', 'infinity_enqueue_front_script_styles', 1 );
		function infinity_enqueue_front_script_styles() {
		wp_enqueue_script( 'infinity-front-scripts', get_stylesheet_directory_uri() . '/js/front-page.min.js', array( 'jquery' ), '1.0.0', true );
		}
}

function flexible() { 
	echo '<h2 class="screen-reader-text">' . __( 'Main Content', 'infinity' ) . '</h2>';
	?>

<?php if (have_posts()) : while (have_posts()) : the_post();

if( have_rows('front_page') ): 
	// loop through all the rows of flexible content
		while ( have_rows('front_page') ) : the_row();
	
	// INTRO
		if( get_row_layout() == 'home_intro' )
			get_template_part('partials/stripe', 'intro');

	// ABOUT US
		if( get_row_layout() == 'about_us' )
			get_template_part('partials/stripe', 'about');

	// IMAGE SEPERATOR
		if( get_row_layout() == 'seperator' )
			get_template_part('partials/stripe', 'seperator');

	// PRODUCTS
		if( get_row_layout() == 'products' )
			get_template_part('partials/stripe', 'products');

	// FREE SHIPPING
		if( get_row_layout() == 'shipping' )
			get_template_part('partials/stripe', 'shipping');



endwhile; // close the loop of flexible content
	endif; // close flexible content conditional

endwhile; endif; // close the WordPress loop 

?>

<?php }


//* Run the Genesis loop
genesis();




