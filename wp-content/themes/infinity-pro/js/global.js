jQuery(document).ready(function($) {

	// Match height for content and sidebar
	$( '.content, .sidebar' ).matchHeight();

});

jQuery(function($) {

	// Set offscreen container height
	var windowHeight = $(window).height();

	$( '.offscreen-container' ).css({
		'height': windowHeight + 'px'
	});

	if ( ( 'relative' !== $( '.js .nav-primary' ).css( 'position' ) ) ) {
		var headerHeight = $( '.site-header' ).height();
		$( '.site-inner' ).not( '.front-page .site-inner' ).css( 'margin-top', headerHeight+'px' );
	} else {
		$( '.site-inner' ).removeAttr( 'style' );
	}

	$(window).resize(function() {

		var windowHeight = $(window).height();

		$( '.offscreen-container' ).css({
			'height': windowHeight + 'px'
		});

		if ( ( 'relative' !== $( '.js .nav-primary' ).css( 'position' ) ) ) {
			var headerHeight = $( '.site-header' ).height();
			$( '.site-inner' ).not( '.front-page .site-inner' ).css( 'margin-top', headerHeight+'px' );
		} else {
			$( '.site-inner' ).removeAttr( 'style' );
		}

	});

	// Add white class to site header after 50px
	$(document).on( 'scroll', function() {

		if ( $(document).scrollTop() > 0 ) {
			$( '.site-container' ).addClass( 'white' );

		} else {
			$( '.site-container' ).removeClass( 'white' );
		}

	});

	// Set offscreen content variables
	var body = $( 'body' ),
		content = $( '.offscreen-content' ),
		sOpen = false;

	// Toggle the offscreen content widget area
	$(document).ready(function() {

		$( '.offscreen-content-toggle' ).click(function() {
			__toggleOffscreenContent();
		});

	});

	function __toggleOffscreenContent() {

		if (sOpen) {
			content.fadeOut();
			body.toggleClass( 'no-scroll' );
			sOpen = false;
		} else {
			content.fadeIn();
			body.toggleClass( 'no-scroll' );
			sOpen = true;
		}

	}

});

// Reponsive Menu

jQuery(document).ready(function($){
        $('.quantity').on('click', '.plus', function(e) {
            $input = $(this).prev('input.qty');
            var val = parseInt($input.val());
            $input.val( val+1 ).change();
        });

        $('.quantity').on('click', '.minus',
            function(e) {
            $input = $(this).next('input.qty');
            var val = parseInt($input.val());
            if (val > 0) {
                $input.val( val-1 ).change();
            }
        });
    });
jQuery(document).ready(function($) {

    $(function() {
  var $menu = $("#genesis-nav-primary").mmenu({
   "extensions": ["effect-menu-zoom",  "pagedim-black",  "effect-listitems-slide", "theme-dark"],
 }, {
 // configuration:
 clone : true,

});


var $icon = $("#my-icon");
var API = $menu.data( "mmenu" );
//var API = $("#genesis-nav-primary").data( "mmenu" );


$icon.on( "click", function() {
   API.open();
});

API.bind( "opened", function() {
   setTimeout(function() {
      $icon.addClass( "is-active" );
   }, 1);
});
API.bind( "closed", function() {
   setTimeout(function() {
      $icon.removeClass( "is-active" );
   }, 1);
});
});
});

// Return To Top

jQuery(document).ready(function($){
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
        );
    });
});
