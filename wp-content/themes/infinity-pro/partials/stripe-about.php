<?php
$image = get_sub_field('image');
$size = 'medium'; // (thumbnail, medium, large, full or custom size)
?>

<section id="front-page-2" class="front-page-2">
<div class="image-section widget-area fadeup-effect">
<div class="wrap">
	<h3 class='widgettitle widget-title'><? the_sub_field('title'); ?></h3>
	<? if( $image ) { echo wp_get_attachment_image( $image, $size ); } ?>
	<? the_sub_field('about_us_info'); ?>
</div>
</div>
</section>