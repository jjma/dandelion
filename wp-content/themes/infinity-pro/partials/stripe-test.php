<?php
$hero_image = get_sub_field('background_image');
if ($hero_image) {
	$hero_bg = 'style="background-image: url(' . $hero_image['url'] . '); background-size:cover;"';
}
$hero_text = get_sub_field('about_us');
$coltwo = get_sub_field('more');
?>

<div class="hero-stripe front-page-2" <?php echo $hero_bg; ?>>
<?php
if ($hero_text) {
					echo $hero_text;
				}

if ($coltwo) {
				 // echo $coltwo;
				echo '<div class="front-page-1"> ' . $coltwo . ' </div>';
}

?>			
</div>

