<?php
$image = get_sub_field('image');
$size = 'custom-thumb-1'; // (thumbnail, medium, large, full or custom size)
//$potentialLink = get_sub_field('link');

//logger("potentialLink: ".$potentialLink, "LINK");
//	function logger($e=null, $level='INFO'){

				 
				   // $errorEmail="jjma100@gmail.com";
					//	$logpath='wp-content/themes/infinity-pro/partials/errors.log';
					//	$fp = fopen ($logpath, "a");
					//	$date=date( "Y-m-d H:i:s" );
					//	$logstring=$date.": [$level]: ".$e."\n";
					//	fwrite($fp, $logstring);
					//	fclose($fp);
			//			if ($level=='CRIT'){ mail($errorEmail,"ERROR: Error from single-courses.php (St Thomas of Aquins)","$logstring"); 		}
//		}


?>

<section id="front-page-4" class="front-page-4" <?php echo $hero_bg; ?>>
<div class="image-section widget-area fadeup-effect">
<div class="wrap">
	<h3 class='widgettitle widget-title'><? the_sub_field('title'); ?></h3>
	<? the_sub_field('product_info'); ?>

<?php if( have_rows('products') ): ?>
	<section class="products-wrapper">
	<ul class="products">

	<?php while( have_rows('products') ): the_row(); 

		$image = get_sub_field('images');
		$size = 'thumbnail';
		?>

		<li class="product">
	
		<? 

 		
 		$potentialLink = get_sub_field('link');
 		
		if ($potentialLink!='') {

	
			?>	

			<a href="<?php echo $potentialLink; ?>">
				<?
					if( $image ) { 
						//logger("Image exists [OK]", "INFO");
						echo wp_get_attachment_image( $image, $size ); 


					}
				

				?>
				</a><span class="products_info"><a href="<?php echo $potentialLink; ?>"><?php the_sub_field('product_info'); ?></a></span>
			<?
		
		} else {
			//logger("Image Doesnt exist [FAIL]", "INFO");
			
			 	if( $image ) { echo wp_get_attachment_image( $image, $size ); } 
			 ?><span class="products_info"><?php the_sub_field('product_info'); ?></span>
	<? } ?>
	</li>

	<?php endwhile; ?>


	</ul>
</section>
<?php endif; ?>	
<a href="/shop" class="button shop">View our online shop</a>
</div>
</div>
</section>
