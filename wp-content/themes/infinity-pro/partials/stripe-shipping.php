<?php

$hero_image = get_sub_field('background_image');

if ($hero_image) {
	$hero_bg = 'style="background-image: url(' . $hero_image['url'] . '); background-size:cover;"';
}

$hero_text 	= get_sub_field('strapline');

?>

<section id="front-page-1" class="front-page-1" <?php echo $hero_bg; ?>>
<div class="image-section widget-area fadeup-effect">
<div class="wrap">',
<?php
	
	if ($hero_text!='') { echo '<div class="shipping"> ' . $hero_text . ' <i class="fa fa-truck" aria-hidden="true"></i></div>';  	}

?>	

</div>
</section>