CREATE TABLE `wp_woocommerce_shipping_zone_methods` (  `zone_id` bigint(20) NOT NULL,  `instance_id` bigint(20) NOT NULL AUTO_INCREMENT,  `method_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `method_order` bigint(20) NOT NULL,  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',  PRIMARY KEY (`instance_id`)) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_woocommerce_shipping_zone_methods` DISABLE KEYS */;
INSERT INTO `wp_woocommerce_shipping_zone_methods` VALUES('1', '2', 'table_rate', '2', '1');
INSERT INTO `wp_woocommerce_shipping_zone_methods` VALUES('2', '3', 'table_rate', '1', '1');
INSERT INTO `wp_woocommerce_shipping_zone_methods` VALUES('1', '5', 'free_shipping', '1', '1');
/*!40000 ALTER TABLE `wp_woocommerce_shipping_zone_methods` ENABLE KEYS */;
