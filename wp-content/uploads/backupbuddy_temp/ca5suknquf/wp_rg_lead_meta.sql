CREATE TABLE `wp_rg_lead_meta` (  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `form_id` mediumint(8) unsigned NOT NULL DEFAULT '0',  `lead_id` bigint(20) unsigned NOT NULL,  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `meta_value` longtext COLLATE utf8mb4_unicode_ci,  PRIMARY KEY (`id`),  KEY `meta_key` (`meta_key`(191)),  KEY `lead_id` (`lead_id`),  KEY `form_id_meta_key` (`form_id`,`meta_key`(191))) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_rg_lead_meta` DISABLE KEYS */;
INSERT INTO `wp_rg_lead_meta` VALUES('1', '2', '1', 'gravityformsmailchimp_is_fulfilled', '1');
INSERT INTO `wp_rg_lead_meta` VALUES('2', '2', '1', 'processed_feeds', 'a:1:{s:21:\"gravityformsmailchimp\";a:1:{i:0;s:1:\"1\";}}');
/*!40000 ALTER TABLE `wp_rg_lead_meta` ENABLE KEYS */;
