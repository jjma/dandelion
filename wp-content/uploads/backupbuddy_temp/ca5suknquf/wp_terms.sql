CREATE TABLE `wp_terms` (  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `term_group` bigint(10) NOT NULL DEFAULT '0',  PRIMARY KEY (`term_id`),  KEY `slug` (`slug`(191)),  KEY `name` (`name`(191))) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES('1', 'Uncategorised', 'uncategorised', '0');
INSERT INTO `wp_terms` VALUES('2', 'simple', 'simple', '0');
INSERT INTO `wp_terms` VALUES('3', 'grouped', 'grouped', '0');
INSERT INTO `wp_terms` VALUES('4', 'variable', 'variable', '0');
INSERT INTO `wp_terms` VALUES('5', 'external', 'external', '0');
INSERT INTO `wp_terms` VALUES('6', 'Candles', 'candles', '0');
INSERT INTO `wp_terms` VALUES('7', 'Soy Wax', 'soy-wax', '0');
INSERT INTO `wp_terms` VALUES('8', 'Beeswax Candles', 'organica-beeswax', '0');
INSERT INTO `wp_terms` VALUES('9', 'Soy Wax Candles', 'soy-wax', '0');
INSERT INTO `wp_terms` VALUES('10', 'D-G Candles', 'd-g-candles', '0');
INSERT INTO `wp_terms` VALUES('11', 'Dalit', 'dalit', '0');
INSERT INTO `wp_terms` VALUES('12', 'Beeswax Candles', 'beeswax-candles', '0');
INSERT INTO `wp_terms` VALUES('13', 'Coconut Milk', 'coconut-milk', '0');
INSERT INTO `wp_terms` VALUES('14', 'Chai Latte', 'chai-latte', '0');
INSERT INTO `wp_terms` VALUES('15', 'Cardamom Coffee', 'cardamom-coffee', '0');
INSERT INTO `wp_terms` VALUES('16', 'Blossom', 'blossom', '0');
INSERT INTO `wp_terms` VALUES('17', 'Kewra', 'kewra', '0');
INSERT INTO `wp_terms` VALUES('18', 'Sandal', 'sandal', '0');
INSERT INTO `wp_terms` VALUES('19', 'Tulsi', 'tulsi', '0');
INSERT INTO `wp_terms` VALUES('20', 'Rose', 'rose', '0');
INSERT INTO `wp_terms` VALUES('21', 'Lemongrass', 'lemongrass', '0');
INSERT INTO `wp_terms` VALUES('22', 'Cinnamon Orange', 'cinnamon-orange', '0');
INSERT INTO `wp_terms` VALUES('23', 'Peppermint Lime &amp; Basil', 'peppermint-lime-basil', '0');
INSERT INTO `wp_terms` VALUES('24', 'Coconut Butter', 'coconut-butter', '0');
INSERT INTO `wp_terms` VALUES('25', 'Lavender', 'lavender', '0');
INSERT INTO `wp_terms` VALUES('26', '10cm x 5cm', '10cm-x-5cm', '0');
INSERT INTO `wp_terms` VALUES('27', '10cm x 10cm', '10cm-x-10cm', '0');
INSERT INTO `wp_terms` VALUES('28', '6.5cm x 7cm', '6-5cm-x-7cm', '0');
INSERT INTO `wp_terms` VALUES('29', '5cm x 5.5cm', '5cm-x-5-5cm', '0');
INSERT INTO `wp_terms` VALUES('30', '3.5cm x 3cm', '3-5cm-x-3cm', '0');
INSERT INTO `wp_terms` VALUES('31', 'Cranberry &amp; Orange', 'cranberry-orange', '0');
INSERT INTO `wp_terms` VALUES('32', 'Fig &amp; Almond', 'fig-almond', '0');
INSERT INTO `wp_terms` VALUES('33', 'Ginger &amp; Lime', 'ginger-lime', '0');
INSERT INTO `wp_terms` VALUES('34', 'Sandalwood Forest', 'sandalwood-forest', '0');
INSERT INTO `wp_terms` VALUES('35', 'Spiced Apple', 'spiced-apple', '0');
INSERT INTO `wp_terms` VALUES('36', 'Bergamot &amp; Ginger', 'bergamot-ginger', '0');
INSERT INTO `wp_terms` VALUES('37', 'Bees Feast', 'bees-feast', '0');
INSERT INTO `wp_terms` VALUES('38', 'Butterfly Delight', 'butterfly-delight', '0');
INSERT INTO `wp_terms` VALUES('39', 'Summer Meadow', 'summer-meadow', '0');
INSERT INTO `wp_terms` VALUES('40', 'Wild Herbs', 'wild-herbs', '0');
INSERT INTO `wp_terms` VALUES('41', 'Sunflowers', 'sunflowers', '0');
INSERT INTO `wp_terms` VALUES('42', 'Cornfield In Bloom', 'cornfield-in-bloom', '0');
INSERT INTO `wp_terms` VALUES('43', 'Italian Garden', 'italian-garden', '0');
INSERT INTO `wp_terms` VALUES('44', 'Daisies', 'daisies', '0');
INSERT INTO `wp_terms` VALUES('45', 'Flower Bombs', 'flower-bombs', '0');
INSERT INTO `wp_terms` VALUES('46', 'Socks', 'socks', '0');
INSERT INTO `wp_terms` VALUES('47', 'Pants', 'pants', '0');
INSERT INTO `wp_terms` VALUES('48', 'Flower Bombs', 'flower-bombs', '0');
INSERT INTO `wp_terms` VALUES('49', 'Socks', 'socks', '0');
INSERT INTO `wp_terms` VALUES('50', 'Pants', 'pants', '0');
INSERT INTO `wp_terms` VALUES('51', 'recycled', 'recycled', '0');
INSERT INTO `wp_terms` VALUES('52', 'jam jars', 'jam-jars', '0');
INSERT INTO `wp_terms` VALUES('53', 'Milk Bottles', 'milk-bottles', '0');
INSERT INTO `wp_terms` VALUES('54', 'Socks Box Set', 'socks-box', '0');
INSERT INTO `wp_terms` VALUES('55', 'Dalit Soap', 'dalit-soap', '0');
INSERT INTO `wp_terms` VALUES('56', 'Turtle Bag', 'turtle-bag', '0');
INSERT INTO `wp_terms` VALUES('57', 'Royal Mail Small Parcel', 'rm-sp', '0');
INSERT INTO `wp_terms` VALUES('59', 'Flower Bombs', 'flower-bombs', '0');
INSERT INTO `wp_terms` VALUES('60', 'City Gardener', 'city-gardener', '0');
INSERT INTO `wp_terms` VALUES('61', 'Organica J', 'organica-j', '0');
INSERT INTO `wp_terms` VALUES('64', 'D&amp;G', 'dg', '0');
INSERT INTO `wp_terms` VALUES('65', 'Soya Wax', 'soya-wax', '0');
INSERT INTO `wp_terms` VALUES('66', 'Sweetpea', 'sweetpea', '0');
INSERT INTO `wp_terms` VALUES('67', 'Bluebell', 'bluebell', '0');
INSERT INTO `wp_terms` VALUES('68', 'Soap', 'soap', '0');
INSERT INTO `wp_terms` VALUES('69', 'Soap', 'soap', '0');
INSERT INTO `wp_terms` VALUES('70', 'Oils', 'oils', '0');
INSERT INTO `wp_terms` VALUES('71', 'Muscle Rub', 'muscle-rub', '0');
INSERT INTO `wp_terms` VALUES('72', 'Herbal Foot Balm', 'herbal-foot-balm', '0');
INSERT INTO `wp_terms` VALUES('73', 'rose', 'rose', '0');
INSERT INTO `wp_terms` VALUES('74', 'Hand Balm', 'hand-balm', '0');
INSERT INTO `wp_terms` VALUES('75', 'Primary Menu', 'primary-menu', '0');
INSERT INTO `wp_terms` VALUES('76', 'Burn Time', 'burn-time', '0');
INSERT INTO `wp_terms` VALUES('77', 'Shoes', 'shoes', '0');
INSERT INTO `wp_terms` VALUES('78', 'D&amp;G Candles', 'dg-candles', '0');
INSERT INTO `wp_terms` VALUES('79', 'Beeswax', 'beeswax', '0');
INSERT INTO `wp_terms` VALUES('80', 'Dalit', 'dalit', '0');
INSERT INTO `wp_terms` VALUES('81', 'Curcuma', 'curcuma', '0');
INSERT INTO `wp_terms` VALUES('82', 'Blossom', 'blossom', '0');
INSERT INTO `wp_terms` VALUES('83', 'Ginger', 'ginger', '0');
INSERT INTO `wp_terms` VALUES('84', 'Cranberry &amp; Orange', 'cranberry-orange', '0');
INSERT INTO `wp_terms` VALUES('85', 'Sandalwood Forest', 'sandalwood-forest', '0');
INSERT INTO `wp_terms` VALUES('86', 'Sweetpea', 'sweetpea', '0');
INSERT INTO `wp_terms` VALUES('87', 'Bluebell', 'bluebell', '0');
INSERT INTO `wp_terms` VALUES('88', 'Lavender', 'lavender', '0');
INSERT INTO `wp_terms` VALUES('89', 'Ginger &amp; Lime', 'ginger-lime', '0');
INSERT INTO `wp_terms` VALUES('90', 'Spiced Apple', 'spiced-apple', '0');
INSERT INTO `wp_terms` VALUES('91', 'Bergamot &amp; Ginger', 'bergamot-ginger', '0');
INSERT INTO `wp_terms` VALUES('92', 'Fig &amp; Almond', 'fig-almond', '0');
INSERT INTO `wp_terms` VALUES('93', 'Tulsi', 'tulsi', '0');
INSERT INTO `wp_terms` VALUES('94', 'Sandal', 'sandal', '0');
INSERT INTO `wp_terms` VALUES('95', 'Kewra', 'kewra', '0');
INSERT INTO `wp_terms` VALUES('96', 'Bees Feast', 'bees-feast', '0');
INSERT INTO `wp_terms` VALUES('97', 'Bamboo Socks', 'bamboo-socks', '0');
INSERT INTO `wp_terms` VALUES('98', 'Forest Stripe', 'forest-stripe', '0');
INSERT INTO `wp_terms` VALUES('99', 'Men', 'men', '0');
INSERT INTO `wp_terms` VALUES('100', 'Women', 'women', '0');
INSERT INTO `wp_terms` VALUES('101', 'Raspberry Dot', 'raspberry-dot', '0');
INSERT INTO `wp_terms` VALUES('102', 'Woman', 'woman', '0');
INSERT INTO `wp_terms` VALUES('103', 'Plum', 'plum', '0');
INSERT INTO `wp_terms` VALUES('104', 'Mustard Spot', 'mustard-spot', '0');
INSERT INTO `wp_terms` VALUES('105', 'Plum Stripe', 'plum-stripe', '0');
INSERT INTO `wp_terms` VALUES('106', 'Forest Dot', 'forest-dot', '0');
INSERT INTO `wp_terms` VALUES('107', 'Raspberry Stripe', 'raspberry-stripe', '0');
INSERT INTO `wp_terms` VALUES('108', 'Sunflowers', 'sunflowers', '0');
INSERT INTO `wp_terms` VALUES('109', 'Mustard', 'mustard', '0');
INSERT INTO `wp_terms` VALUES('110', 'Raspberry', 'raspberry', '0');
INSERT INTO `wp_terms` VALUES('111', 'Plum Dot', 'plum-dot', '0');
INSERT INTO `wp_terms` VALUES('112', 'Ocean Pattern', 'ocean-pattern', '0');
INSERT INTO `wp_terms` VALUES('113', 'Men', 'men', '0');
INSERT INTO `wp_terms` VALUES('114', 'Rust Pattern', 'rust-pattern', '0');
INSERT INTO `wp_terms` VALUES('115', 'Ocean Stripe', 'ocean-stripe', '0');
INSERT INTO `wp_terms` VALUES('116', 'Navy Pattern', 'navy-pattern', '0');
INSERT INTO `wp_terms` VALUES('117', 'Burnt Orange Stripe', 'burnt-orange-stripe', '0');
INSERT INTO `wp_terms` VALUES('118', 'Black', 'black', '0');
INSERT INTO `wp_terms` VALUES('119', 'Navy', 'navy', '0');
INSERT INTO `wp_terms` VALUES('120', 'Box Set', 'box-set', '0');
INSERT INTO `wp_terms` VALUES('121', 'Shop Menu', 'shop-menu', '0');
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
