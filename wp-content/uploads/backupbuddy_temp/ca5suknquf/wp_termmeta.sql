CREATE TABLE `wp_termmeta` (  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `meta_value` longtext COLLATE utf8mb4_unicode_ci,  PRIMARY KEY (`meta_id`),  KEY `term_id` (`term_id`),  KEY `meta_key` (`meta_key`(191))) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
INSERT INTO `wp_termmeta` VALUES('1', '6', 'order', '1');
INSERT INTO `wp_termmeta` VALUES('2', '6', 'product_count_product_cat', '13');
INSERT INTO `wp_termmeta` VALUES('3', '7', 'product_count_product_tag', '2');
INSERT INTO `wp_termmeta` VALUES('4', '11', 'product_count_product_tag', '7');
INSERT INTO `wp_termmeta` VALUES('5', '12', 'product_count_product_tag', '2');
INSERT INTO `wp_termmeta` VALUES('6', '13', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('7', '14', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('8', '13', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('9', '13', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('10', '13', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('11', '13', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('12', '13', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('13', '13', 'description', '');
INSERT INTO `wp_termmeta` VALUES('14', '13', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('15', '13', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('16', '13', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('17', '13', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('18', '13', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('19', '15', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('20', '16', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('21', '17', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('22', '18', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('23', '19', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('24', '20', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('25', '21', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('26', '22', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('27', '23', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('28', '24', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('29', '25', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('30', '26', 'order_pa_dimensions', '0');
INSERT INTO `wp_termmeta` VALUES('31', '27', 'order_pa_dimensions', '0');
INSERT INTO `wp_termmeta` VALUES('32', '28', 'order_pa_dimensions', '0');
INSERT INTO `wp_termmeta` VALUES('33', '29', 'order_pa_dimensions', '0');
INSERT INTO `wp_termmeta` VALUES('34', '30', 'order_pa_dimensions', '0');
INSERT INTO `wp_termmeta` VALUES('35', '31', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('36', '32', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('37', '33', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('38', '34', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('39', '35', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('40', '36', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('41', '37', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('42', '38', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('43', '39', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('44', '40', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('45', '41', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('46', '42', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('47', '43', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('48', '44', 'order_pa_flower', '0');
INSERT INTO `wp_termmeta` VALUES('49', '45', 'order', '4');
INSERT INTO `wp_termmeta` VALUES('50', '46', 'order', '10');
INSERT INTO `wp_termmeta` VALUES('51', '47', 'order', '6');
INSERT INTO `wp_termmeta` VALUES('52', '45', 'product_count_product_cat', '6');
INSERT INTO `wp_termmeta` VALUES('53', '51', 'product_count_product_tag', '2');
INSERT INTO `wp_termmeta` VALUES('54', '52', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('55', '53', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('56', '59', 'product_count_product_tag', '6');
INSERT INTO `wp_termmeta` VALUES('57', '60', 'product_count_product_tag', '6');
INSERT INTO `wp_termmeta` VALUES('58', '61', 'product_count_product_tag', '4');
INSERT INTO `wp_termmeta` VALUES('83', '64', 'product_count_product_tag', '9');
INSERT INTO `wp_termmeta` VALUES('84', '65', 'product_count_product_tag', '9');
INSERT INTO `wp_termmeta` VALUES('85', '66', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('86', '67', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('87', '68', 'order', '8');
INSERT INTO `wp_termmeta` VALUES('88', '68', 'product_count_product_cat', '7');
INSERT INTO `wp_termmeta` VALUES('89', '69', 'product_count_product_tag', '7');
INSERT INTO `wp_termmeta` VALUES('90', '70', 'order', '5');
INSERT INTO `wp_termmeta` VALUES('91', '70', 'product_count_product_cat', '3');
INSERT INTO `wp_termmeta` VALUES('92', '71', 'product_count_product_tag', '2');
INSERT INTO `wp_termmeta` VALUES('93', '72', 'product_count_product_tag', '0');
INSERT INTO `wp_termmeta` VALUES('94', '73', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('95', '74', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('96', '46', 'product_count_product_cat', '20');
INSERT INTO `wp_termmeta` VALUES('97', '75', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('98', '75', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('99', '75', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('100', '75', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('101', '75', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('102', '75', 'description', '');
INSERT INTO `wp_termmeta` VALUES('103', '75', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('104', '75', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('105', '75', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('106', '75', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('107', '75', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('108', '6', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('109', '6', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('110', '6', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('111', '6', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('112', '6', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('113', '6', 'description', '');
INSERT INTO `wp_termmeta` VALUES('114', '6', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('115', '6', 'layout', 'content-sidebar');
INSERT INTO `wp_termmeta` VALUES('116', '6', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('117', '6', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('118', '6', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('119', '6', 'display_type', '');
INSERT INTO `wp_termmeta` VALUES('120', '6', 'thumbnail_id', '190');
INSERT INTO `wp_termmeta` VALUES('121', '76', 'order_pa_scent', '0');
INSERT INTO `wp_termmeta` VALUES('122', '77', 'order', '7');
INSERT INTO `wp_termmeta` VALUES('123', '77', 'display_type', '');
INSERT INTO `wp_termmeta` VALUES('124', '77', 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES('125', '77', 'product_count_product_cat', '0');
INSERT INTO `wp_termmeta` VALUES('126', '45', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('127', '45', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('128', '45', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('129', '45', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('130', '45', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('131', '45', 'description', '');
INSERT INTO `wp_termmeta` VALUES('132', '45', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('133', '45', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('134', '45', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('135', '45', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('136', '45', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('137', '45', 'display_type', 'products');
INSERT INTO `wp_termmeta` VALUES('138', '45', 'thumbnail_id', '152');
INSERT INTO `wp_termmeta` VALUES('139', '78', 'order', '3');
INSERT INTO `wp_termmeta` VALUES('140', '78', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('141', '78', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('142', '78', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('143', '78', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('144', '78', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('145', '78', 'description', '');
INSERT INTO `wp_termmeta` VALUES('146', '78', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('147', '78', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('148', '78', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('149', '78', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('150', '78', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('151', '78', 'display_type', '');
INSERT INTO `wp_termmeta` VALUES('152', '78', 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES('153', '79', 'order', '2');
INSERT INTO `wp_termmeta` VALUES('154', '79', 'product_count_product_cat', '2');
INSERT INTO `wp_termmeta` VALUES('155', '78', 'product_count_product_cat', '9');
INSERT INTO `wp_termmeta` VALUES('156', '80', 'order', '9');
INSERT INTO `wp_termmeta` VALUES('157', '80', 'product_count_product_cat', '6');
INSERT INTO `wp_termmeta` VALUES('158', '81', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('159', '82', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('160', '83', 'product_count_product_tag', '0');
INSERT INTO `wp_termmeta` VALUES('161', '84', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('162', '85', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('163', '86', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('164', '87', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('165', '88', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('166', '89', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('167', '90', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('168', '91', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('169', '92', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('170', '93', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('171', '94', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('172', '95', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('173', '96', 'product_count_product_tag', '6');
INSERT INTO `wp_termmeta` VALUES('174', '97', 'product_count_product_tag', '20');
INSERT INTO `wp_termmeta` VALUES('175', '98', 'product_count_product_tag', '2');
INSERT INTO `wp_termmeta` VALUES('176', '99', 'order', '11');
INSERT INTO `wp_termmeta` VALUES('177', '99', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('178', '99', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('179', '99', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('180', '99', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('181', '99', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('182', '99', 'description', '');
INSERT INTO `wp_termmeta` VALUES('183', '99', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('184', '99', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('185', '99', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('186', '99', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('187', '99', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('188', '99', 'display_type', '');
INSERT INTO `wp_termmeta` VALUES('189', '99', 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES('190', '100', 'order', '12');
INSERT INTO `wp_termmeta` VALUES('191', '100', 'display_type', '');
INSERT INTO `wp_termmeta` VALUES('192', '100', 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES('193', '99', 'product_count_product_cat', '9');
INSERT INTO `wp_termmeta` VALUES('194', '100', 'product_count_product_cat', '11');
INSERT INTO `wp_termmeta` VALUES('195', '101', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('196', '102', 'product_count_product_tag', '11');
INSERT INTO `wp_termmeta` VALUES('197', '103', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('198', '104', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('199', '105', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('200', '106', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('201', '107', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('202', '108', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('203', '109', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('204', '110', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('205', '111', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('206', '112', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('207', '113', 'product_count_product_tag', '9');
INSERT INTO `wp_termmeta` VALUES('208', '114', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('209', '115', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('210', '116', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('211', '117', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('212', '118', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('213', '119', 'product_count_product_tag', '1');
INSERT INTO `wp_termmeta` VALUES('214', '120', 'product_count_product_tag', '2');
INSERT INTO `wp_termmeta` VALUES('215', '79', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('216', '79', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('217', '79', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('218', '79', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('219', '79', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('220', '79', 'description', '');
INSERT INTO `wp_termmeta` VALUES('221', '79', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('222', '79', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('223', '79', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('224', '79', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('225', '79', 'noarchive', '0');
INSERT INTO `wp_termmeta` VALUES('226', '79', 'display_type', '');
INSERT INTO `wp_termmeta` VALUES('227', '79', 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES('228', '121', 'headline', '');
INSERT INTO `wp_termmeta` VALUES('229', '121', 'intro_text', '');
INSERT INTO `wp_termmeta` VALUES('230', '121', 'display_title', '0');
INSERT INTO `wp_termmeta` VALUES('231', '121', 'display_description', '0');
INSERT INTO `wp_termmeta` VALUES('232', '121', 'doctitle', '');
INSERT INTO `wp_termmeta` VALUES('233', '121', 'description', '');
INSERT INTO `wp_termmeta` VALUES('234', '121', 'keywords', '');
INSERT INTO `wp_termmeta` VALUES('235', '121', 'layout', '');
INSERT INTO `wp_termmeta` VALUES('236', '121', 'noindex', '0');
INSERT INTO `wp_termmeta` VALUES('237', '121', 'nofollow', '0');
INSERT INTO `wp_termmeta` VALUES('238', '121', 'noarchive', '0');
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
