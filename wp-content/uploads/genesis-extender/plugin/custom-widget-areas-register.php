<?php
/**
 * Register Custom Widget Areas.
 */

genesis_register_sidebar( array(
	'id' 			=>	'site_social_links',
	'name'			=>	__( 'Site Social Links', 'extender' ),
	'description' 	=>	__( 'The social media links section', 'extender' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'map',
	'name'			=>	__( 'map', 'extender' ),
	'description' 	=>	__( '', 'extender' )
) );
