<?php
/**
 * Build and Hook-In Custom Hook Boxes.
 */

/* Name: To Top */

add_action( 'genesis_before', 'genesis_extender_to_top_hook_box', 10 );
function genesis_extender_to_top_hook_box() {
	genesis_extender_to_top_hook_box_content();
}

function genesis_extender_to_top_hook_box_content() { ?>
<a href="#0" class="cd-top">Top</a> 
<?php
}

/* Name: map-hook */

add_action( 'genesis_before_content_sidebar_wrap', 'genesis_extender_map_hook_hook_box', 10 );
function genesis_extender_map_hook_hook_box() {
	genesis_extender_map_hook_hook_box_content();
}

function genesis_extender_map_hook_hook_box_content() {
	if ( extender_has_label('contact') ) { ?>
<div class="map-wrapper"><a name="location"></a><?php echo do_shortcode( '[map]' ); ?></div>
	<?php } else {
		return false;
	}
}

/* Name: Shipping */

add_action( 'genesis_before_content_sidebar_wrap', 'genesis_extender_shipping_hook_box', 1 );
function genesis_extender_shipping_hook_box() {
	genesis_extender_shipping_hook_box_content();
}

function genesis_extender_shipping_hook_box_content() {
	if ( ! is_page(92) and ! is_front_page() ) { ?>
<div class="shipping">Spend £100 and receive free shipping <i class="fa fa-truck" aria-hidden="true"></i></div>
	<?php } else {
		return false;
	}
}
