<?php
/**
 * Build and Hook-In Custom Widget Areas.
 */

/* Name: Site Social Links */

add_action( 'genesis_after_content', 'genesis_extender_site_social_links_widget_area', 10 );
function genesis_extender_site_social_links_widget_area() {
	genesis_extender_site_social_links_widget_area_content();
}

function genesis_extender_site_social_links_widget_area_content() {
	genesis_widget_area( 'site_social_links', $args = array (
		'before'              => '<div id="site_social_links" class="widget-area genesis-extender-widget-area site-social">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_site_social_links_widget_area',
		'after_sidebar_hook'  => 'genesis_after_site_social_links_widget_area'
	) );
}

/* Name: map */

add_shortcode( 'map', 'genesis_extender_map_widget_area_shortcode' );
function genesis_extender_map_widget_area_shortcode() {
	ob_start();
	genesis_extender_map_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function genesis_extender_map_widget_area_content() {
	if ( extender_has_label('contact') ) {
		genesis_widget_area( 'map', $args = array (
			'before'              => '<div id="map" class="widget-area genesis-extender-widget-area gmap">',
			'after'               => '</div>',
			'before_sidebar_hook' => 'genesis_before_map_widget_area',
			'after_sidebar_hook'  => 'genesis_after_map_widget_area'
		) );
	} else {
		return false;
	}
}
