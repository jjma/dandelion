��    B      ,  Y   <      �  0   �     �  
   �     �  	   �                *     2  	   >     H     V     d     x     �     �     �     �  	   �     �     �     �  
   �  
   �     �                    ,     8  	   J  	   T     ^     l     u     �  
   �     �     �  	   �     �     �  	   �  	   �     �     �     �               #     0     <     L     S  
   Z     e     m     y  	        �     �     �     �  1   �     �  -  �  0   &
     W
  
   d
     o
  	   {
     �
     �
     �
     �
  	   �
     �
     �
     �
               -     6     C  	   J     T     ]     j  
   q  
   |     �     �     �     �     �     �  	   �  	   �     �     �            
              ,  	   F     P     ^  	   d  	   n     x     �     �     �     �     �     �     �     �     �  
   �     �     �       	                  #     +  1   7     i     '      ,       6   :                  (      2   4                 ;   >                     +       @   &   7            *   =      5                           #      3      $           ?      8      B                    9          /   <      	         )   
   %   !   "   -         .          A      0           1          A simple CSS and SVG driven social icons widget. Align Center Align Left Align Right Alignment Background Color: Background Hover Color: Behance Behance URI Bloglovin Bloglovin URI Border Color: Border Hover Color: Border Width: Displays select social icons. Dribbble Dribbble URI Email Email URI Facebook Facebook URI Flickr Flickr URI GitHub URI Github Google+ Google+ URI Icon Border Radius: Icon Color: Icon Hover Color: Icon Size Instagram Instagram URI Linkedin Linkedin URI Medium Medium URI Nathan Rice Open links in new window? Periscope Periscope URI Phone Phone URI Pinterest Pinterest URI RSS RSS URI Simple Social Icons Snapchat Snapchat URI StumbleUpon StumbleUpon URI Title: Tumblr Tumblr URI Twitter Twitter URI Vimeo Vimeo URI Xing Xing URI YouTube YouTube URI http://wordpress.org/plugins/simple-social-icons/ http://www.nathanrice.net/ PO-Revision-Date: 2016-12-06 20:42:56+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-alpha
Language: en_GB
Project-Id-Version: Plugins - Simple Social Icons - Stable (latest release)
 A simple CSS and SVG-driven social icons widget. Align Centre Align left Align right Alignment Background Colour: Background Hover Colour: Behance Behance URI Bloglovin Bloglovin URI Border Colour: Border Hover Colour: Border Width: Displays select social icons. Dribbble Dribbble URI E-mail Email URI Facebook Facebook URI Flickr Flickr URI GitHub URI Github Google+ Google+ URI Icon Border Radius: Icon Colour: Icon Hover Colour: Icon Size Instagram Instagram URI Linkedin LinkedIn URI Medium Medium URI Nathan Rice Open links in new window? Periscope Periscope URI Phone Phone URI Pinterest Pinterest URI RSS RSS URI Simple Social Icons Snapchat Snapchat URI StumbleUpon StumbleUpon URI Title: Tumblr Tumblr URI Twitter Twitter URI Vimeo Vimeo URI Xing Xing URI YouTube YouTube URI http://wordpress.org/plugins/simple-social-icons/ http://www.nathanrice.net/ 