<?php

/*
* Plugin Name: Dandelion & Ginger Site Functionality Plugin
* Description: All of the important functionality of your site belongs in this. Do not deactivate or the site will break..
* Version: 1
* License: GPL
* Author: Launch Site - Jonathan Alstead
* Author URI: www.launchsite.co.uk
*/



/* MEDIA */

/* -- SVG Support --*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/* MOBILE MENU
---------------------------------------------------------------------------------------------------- */


/**********************************
 *
 * Enqueue Style and Scripts for Mmenu.
 *
 * @author AlphaBlossom / Tony Eppright
 * @link http://www.alphablossom.com
 *
 **********************************/
/*add_action( 'genesis_after_footer', 'youruniqueprefix_load_scripts');
function youruniqueprefix_load_scripts() {


    // JS
    wp_register_script( 'the_mmenu_js' , get_stylesheet_directory_uri() . '/js/jquery.mmenu.min.js' , array( 'jquery' ), '4.2.6', FALSE ); // mmenu js
   // wp_register_script( 'the_fixed_mmenu_js' , get_stylesheet_directory_uri() . '/js/jquery.mmenu.fixedelements.min.js' , array( 'jquery' ), '1', FALSE ); // fixed js
   wp_register_script( 'headerscript_js' , get_stylesheet_directory_uri() . '/js/headerscript.js' , array( 'jquery' ), '1.0', FALSE ); // custom header js

    wp_enqueue_script( 'the_mmenu_js');
    //wp_enqueue_script('the_fixed_mmenu_js');
   wp_enqueue_script('headerscript_js');

}*/

/*add_action('genesis_after_footer', 'footercss');
function footercss(){
 // CSS
wp_register_style( 'youruniqueprefix_mmenu_css' , get_stylesheet_directory_uri() . '/jquery.mmenu.css' , null );
wp_register_style('effects_css', get_stylesheet_directory_uri() . '/jquery.mmenu.effects.css' , null );
wp_enqueue_style('youruniqueprefix_mmenu_css');
wp_enqueue_style('effects_css');

}*/


/* Navigation functions */
add_action( 'genesis_header', 'youruniqueprefix_nav_control', 10 );
function youruniqueprefix_nav_control() {
 ?>
 <div id="header">
<div >
<a href="#genesis-nav-primary" class="mini-menu">
<div class="hamburger hamburger--spin" tabindex="0"
     aria-label="Menu" role="button" aria-controls="navigation" id="my-icon">
  <div class="hamburger-box">
    <div class="hamburger-inner"></div>
  </div></div>
</div></a>
</div>
 <?php
}

/* WOOCOMMERCE
---------------------------------------------------------------------------------------------------- */

// Display 20 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );

/**
 * Remove duplicate page-title from WooCommerce archive pages
 * Source: http://envy.krolyn.com/forums/topic/how-do-i-remove-the-page-title-only-on-categoryarchive-pages/
 */

add_filter( 'woocommerce_show_page_title', 'envy_hide_page_titles' );
function envy_hide_page_titles() {
    if ( is_shop() )  // Exclude Shop page
    return true;
}


/**
 * Add WooCommerce My Account Login/Logout to Menu
 *
 * @see https://support.woothemes.com/hc/en-us/articles/203106357-Add-Login-Logout-Links-To-The-Custom-Primary-Menu-Area
 */

function my_account_loginout_link( $items, $args ) {
 if (is_user_logged_in() && $args->theme_location == 'primary') { //change your theme location menu to suit
 $items .= '<li class="menu-item my-account"><a href="'. wp_logout_url( get_permalink( wc_get_page_id( 'shop' ) ) ) .'">Log Out</a></li>'; //change logout link, here it goes to 'shop', you may want to put it to 'myaccount'
 }
 elseif (!is_user_logged_in() && $args->theme_location == 'primary') {//change your theme location menu to suit
 $items .= '<li class="menu-item my-account"><a href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '"><i class="fa fa-user" aria-hidden="true"></i><span>&nbsp;My Account</span></a></li>';
 }
 return $items;
}


add_filter( 'wp_nav_menu_items', 'my_account_loginout_link', 1, 2 );


/* Remove labels from the shopping cart in woocommerce i.e table rate */

add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_remove_shipping_label', 10, 2 );

function bbloomer_remove_shipping_label($label, $method) {
$new_label = preg_replace( '/^.+:/', '', $label );
return $new_label;
}


// rename the "Have a Coupon?" message on the checkout page
function woocommerce_rename_coupon_message_on_checkout() {

  return 'Have a Promo Code?' . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>';
}
add_filter( 'woocommerce_checkout_coupon_message', 'woocommerce_rename_coupon_message_on_checkout' );

// rename the coupon field on the checkout page
function woocommerce_rename_coupon_field_on_checkout( $translated_text, $text, $text_domain ) {

  // bail if not modifying frontend woocommerce text
  if ( is_admin() || 'woocommerce' !== $text_domain ) {
    return $translated_text;
  }

  if ( 'Coupon code' === $text ) {
    $translated_text = 'Promo Code';

  } elseif ( 'Apply Coupon' === $text ) {
    $translated_text = 'Apply Promo Code';
  }

  return $translated_text;
}
add_filter( 'gettext', 'woocommerce_rename_coupon_field_on_checkout', 10, 3 );


/**
 *  Remove Image and links to product from cart
 *
 * Source: https://www.skyverge.com/blog/remove-woocommerce-product-images-from-the-cart/
 */

/*function sv_remove_cart_product_link( $product_link, $cart_item, $cart_item_key ) {
    $product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    return $product->get_title();
}
add_filter( 'woocommerce_cart_item_thumbnail', '__return_false' );
add_filter( 'woocommerce_cart_item_name', 'sv_remove_cart_product_link', 10, 3 );
*/

/*/**
 * Tweak WooCommerce styles and scripts.
 * credit goes to Greg from: https://gist.github.com/gregrickaby/2846416
 */
function grd_woocommerce_script_cleaner() {

    // Remove the generator tag, to reduce WooCommerce based hacking attacks
    remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
    // Unless we're in the store, remove all the scripts and junk!
    if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
        wp_dequeue_style( 'woocommerce_frontend_styles' );
        wp_dequeue_style( 'woocommerce-general');
        wp_dequeue_style( 'woocommerce-layout' );
        wp_dequeue_style( 'woocommerce-smallscreen' );
        wp_dequeue_style( 'woocommerce_fancybox_styles' );
        wp_dequeue_style( 'woocommerce_chosen_styles' );
        wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
        wp_dequeue_style( 'select2' );
        wp_dequeue_script( 'wc-add-payment-method' );
        wp_dequeue_script( 'wc-lost-password' );
        wp_dequeue_script( 'wc_price_slider' );
        wp_dequeue_script( 'wc-single-product' );
        wp_dequeue_script( 'wc-add-to-cart' );
        wp_dequeue_script( 'wc-cart-fragments' );
        wp_dequeue_script( 'wc-credit-card-form' );
        wp_dequeue_script( 'wc-checkout' );
        wp_dequeue_script( 'wc-add-to-cart-variation' );
        wp_dequeue_script( 'wc-single-product' );
        //wp_dequeue_script( 'wc-cart' ); // might need to comment out if you have cart icon on all pages
        wp_dequeue_script( 'wc-chosen' );
        wp_dequeue_script( 'woocommerce' );
        wp_dequeue_script( 'prettyPhoto' );
        wp_dequeue_script( 'prettyPhoto-init' );
        wp_dequeue_script( 'jquery-blockui' );
        wp_dequeue_script( 'jquery-placeholder' );
        wp_dequeue_script( 'jquery-payment' );
        wp_dequeue_script( 'jqueryui' );
        wp_dequeue_script( 'fancybox' );
        wp_dequeue_script( 'wcqi-js' );

    }
}
add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );

//add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Remove each style one by one

add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
    //unset( $enqueue_styles['woocommerce-general'] );  // Remove the gloss
    unset( $enqueue_styles['woocommerce-layout'] );       // Remove the layout
    //unset( $enqueue_styles['woocommerce-smallscreen'] );  // Remove the smallscreen optimisation
    return $enqueue_styles;
}

/* STRUCTURAL */

/*-- Single Product Page - Add Section to Summary ---*/

// attach my open 'section' function to the before product summary action
add_action( 'woocommerce_before_single_product_summary', 'wrapper_start', 20 );
// attach my close 'section' function to the after product summary action
add_action( 'woocommerce_after_single_product_summary', 'wrapper_end', 20 );

//This function opens a new css section tag called "no-image-wrap" to the page HTML before all of the woocommerce "stuff" except images
function wrapper_start() {
 echo '<section id="summary-wrapper">';
}
//This line adds the HTML to the page to end the new "no-image-wrap" section
function wrapper_end() { echo '</section>'; }


